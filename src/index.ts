import { Cluster } from "./cluster"

export default {
  async fetch(request: Request, env: Env) {
    try {
      return await handleRequest(request, env)
    } catch (e) {
      return new Response(`${e}`)
    }
  },
}

async function handleRequest(request: Request, env: Env) {
  const url = new URL(request.url); // /{id}/{client|device}
  const parts = url.pathname.slice(1).split('/');
  if (parts.length < 2) {
    return new Response("Page not found", { status: 404 });
  }

  const id = env.clusters.idFromName(parts[0]);
  const cluster = env.clusters.get(id);
  
  return cluster.fetch(request);
}

interface Env {
  clusters: DurableObjectNamespace
}

export { Cluster }

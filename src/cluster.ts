import { Device, WsMessage } from './models'

interface DeviceRequest {
  cpu: number
  memory: number
}

export class Cluster {
  state: DurableObjectState
  env: Env
  devices: Map<String, Device>
  clients: WebSocket[]

  constructor(state: DurableObjectState, env: Env) {
    this.state = state
    this.env = env
    this.devices = new Map()
    this.clients = []

    this.state.blockConcurrencyWhile(async () => {
      this.devices = (await this.state.storage?.list<Device>()) ?? this.devices
    })
  }

  async fetch(request: Request) {
    const url = new URL(request.url)

    const parts = url.pathname.slice(1).split('/')

    if (parts[1] !== 'client' && parts[1] !== 'device') {
      return new Response('Not found', { status: 404 })
    }

    if (parts[1] === 'device' && parts.length < 3) {
      return new Response('Invalid request, missing device id', { status: 400 })
    }

    if (request.headers.get('Upgrade') != 'websocket') {
      return new Response('expected websocket', { status: 400 })
    }

    try {
      const pair = new WebSocketPair()
      if (url.pathname.endsWith('client')) {
        await this.handleClient(pair[1])
      } else {
        await this.handleDevice(pair[1], parts[2])
      }

      return new Response(null, { status: 101, webSocket: pair[0] })
    } catch {
      return new Response(
        'An error occured while setting the websocket connection',
        { status: 500 },
      )
    }
  }

  async handleClient(webSocket: WebSocket) {
    webSocket.accept()

    this.clients.push(webSocket)

    webSocket.send(JSON.stringify({ devices: [...this.devices.values()] }))

    webSocket.addEventListener(
      'close',
      () => (this.clients = this.clients.filter(c => c !== webSocket)),
    )
  }

  async handleDevice(webSocket: WebSocket, id: string) {
    webSocket.accept()

    let device: Device

    let firstRequest = true
    webSocket.addEventListener('message', async event => {
      try {
        if (firstRequest) {
          device = JSON.parse(event.data as string) as Device
          device.cpuusage = []
          device.memoryusage = []
        } else {
          const data = JSON.parse(event.data as string) as DeviceRequest

          device.cpu = data.cpu
          device.cpuusage = [
            ...device.cpuusage.slice(device.cpuusage.length >= 100 ? 1 : 0),
            data.cpu,
          ]
          device.memory = data.memory
          device.memoryusage = [
            ...device.memoryusage.slice(
              device.memoryusage.length >= 100 ? 1 : 0,
            ),
            data.memory,
          ]
          device.connected = true
        }

        device.id = id
        device.lastUpdate = new Date().toISOString()

        this.devices.set(device.id, device)
        this.state.storage?.put(device.id, device)

        if (firstRequest) {
          firstRequest = false
          this.broadcast({ devices: [device] })
        } else {
          this.broadcast({
            updates: [
              { device_id: id, cpu: device.cpu ?? 0, memory: device.memory ?? 0 },
            ],
          })
        }
      } catch {
        webSocket.send(JSON.stringify({ message: "An error occured" }));
      }
    })

    webSocket.addEventListener('close', () => {
      device.connected = false
      this.devices.set(device.id, device)
      this.broadcast({ devices: [device] })
    })
  }

  async broadcast(message: WsMessage) {
    const msg = JSON.stringify(message)
    this.clients = this.clients.filter(client => {
      try {
        client.send(msg)
        return true
      } catch {
        return false
      }
    })
  }
}

interface Env {}

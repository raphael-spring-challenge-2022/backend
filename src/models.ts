export interface Device {
  id: string;
  lastUpdate: string;
  cores: number;
  cpu: number;
  cpuusage: number[];
  total_memory: number;
  memory: number;
  memoryusage: number[];
  arch: string;
  hostname: string;
  version: string;
  type: string;
  platform: string;
  connected: boolean;
}

export interface Update {
    device_id: string;
    cpu: number;
    memory: number;
}

export interface WsMessage {
    devices?: Device[];
    updates?: Update[];
}
